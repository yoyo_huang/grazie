'use strict';

var scrollNow = $(window).scrollTop();
var nowScene = hashObj.food ? hashObj.food : 1; //套餐編號 1~n
var nowMeal = hashObj.meal ? hashObj.meal : 1; //菜色編號 1~n
var offsetPoint= [];

if ( ! hashObj.meal ){
  $(".section-meal-info").addClass("is-hide");
}

$(window).scroll(function () {
  var scrollVal = $(this).scrollTop();
  var newOffset;
  var main_h = $(".section-main").height();
  var side_h = $(".category-list").height()
  + parseInt( $(".category-list").css("padding-top") )
  + parseInt( $(".nav-category").css("padding-top") );

  var win_h = $(window).height();

  //Mobile UI
  if( ( scrollVal + win_h ) >= $("body").height() - $("#footer").height() ){
    // console.log("over!");
    $(".meal-footer").addClass("style-stick");
  }else{
    $(".meal-footer").removeClass("style-stick");
  }

  //Left-side stick to screen
  if(scrollVal > scrollNow ){
    //when scrolling down
    if( side_h > win_h ){
      //when side is taller than windows : stick bottom
      if( (win_h+scrollVal) > side_h ){
        if( (win_h+scrollVal) <= main_h ){
          $(".category-list").css("margin-top", (scrollVal - side_h + win_h) );
        }
      }
    }else{
      //when side in shorter than windows : stick top
      $(".category-list").css("margin-top", scrollVal );
    }
  }else{
    //when scrolling up : stick top
    var _dis_scroll_up = scrollVal - scrollNow; //minus number
    var now_margin = $(".category-list").css("margin-top");
    now_margin = parseInt(now_margin);
    if( scrollVal <= (now_margin+_dis_scroll_up) ){
      $(".category-list").css("margin-top", scrollVal );
    }
  }


  //Switch left side category-name
  $(".category-name.anchor").each(function(index, el) {
    if( scrollVal + $(window).height()/2 >= offsetPoint[index] ){
      if( scrollVal + $(window).height()/2 < offsetPoint[index+1] ){
        // 0 ~ max-1
        nowScene = index+1 ;
      }
    }
    if( scrollVal + $(window).height()/2 >= offsetPoint[ offsetPoint.length-1 ] ){
      // max
      nowScene = offsetPoint.length ;
    }
  });
  // console.log("nowScene = "+nowScene);
  window.location.hash = "food="+nowScene+"&meal="+nowMeal;
  // footerSwiper.slideTo( nowScene-1 );
  $(".category-item").removeClass('is-active').eq(nowScene-1).addClass("is-active");
  $(".footer-foodlist").find(".swiper-slide").removeClass('is-active').eq(nowScene-1).addClass('is-active');
  scrollNow = scrollVal;
});

$(window).resize(function(){
});


// Click category-item to scroll
$(".category-item").each(function(index, el) {
  $(this).on('click', function(event) {
    event.preventDefault();
    var newScroll = $(".category-name.anchor").eq(index).offset().top;
    $('html,body').animate({ scrollTop:( newScroll )}, 800 );
  });
});


$(window).load(function() {

  // After load, get category offset ( to do scroll event )
  $(".category-name.anchor").each(function(index, el) {
    offsetPoint.push( $(this).offset().top );  
  });

  // Get hash meal tag #0 ~ n-1
  if( hashObj.meal ){
    var mymeal = parseInt( hashObj.meal );
    $(".settype-item").removeClass('is-active').eq(mymeal-1).addClass("is-active");
    bannerSwiper.slideTo( mymeal-1 );
    if ( !($(window).width() > 1024) ){
      $(".meal-banner").addClass("swiper-no-swiping");
    }
    $(".settype-content").removeClass('is-active').eq(mymeal-1).addClass("is-active");
  }else{
    $(".btn-open-mealinfo").remove();
  }

  // Get hash food tag #0 ~ n-1
  if( hashObj.food ){
    var myfood = parseInt(hashObj.food);
    var newScroll = $(".category-name.anchor").eq( myfood-1 ).offset().top;
    $('html,body').animate({ scrollTop:( newScroll )}, 800 );
    $(".footer-foodlist").find(".swiper-slide").removeClass('is-active').eq(myfood-1).addClass('is-active');
  }else{
    $('html,body').animate({ scrollTop:( 0 )}, 800);
  }

});


var footerSwiper = new Swiper('.footer-foodlist', {
  nextButton: '.footer-foodlist .swiper-button-next',
  prevButton: '.footer-foodlist .swiper-button-prev',
  spaceBetween: 0,
  initialSlide: 1,
  loop: false,
  speed: 400,
  centeredSlides: true,
  slidesPerView: $(window).width() > 320 ? 3 : 2,
});   

var bannerSwiper = new Swiper('.meal-banner', {
  effect: "fade",
  simulateTouch: false,
  speed: 1000,
  spaceBetween: 0
});




(function() {

  console.log(hashObj);


  $(".settype-list").find("li").each(function(index, el) {
    $(this).on('click', 'span', function(event) {
      event.preventDefault();
      /* Act on the event */
      $(".settype-item").removeClass('is-active').eq(index).addClass("is-active");
      $(".settype-content").removeClass('is-active').eq(index).addClass("is-active");
      bannerSwiper.slideTo( index );
      nowMeal = index+1 ;
      window.location.hash = "food="+nowScene+"&meal="+nowMeal;
      $('.section-meal-info .inner').perfectScrollbar('update');
    }); 
  });

  $(".setname-list").each(function(i, el) {
    $(this).find(".setname-item").each(function(j, el) {
      $(this).on('click touch', function(event) {
        // event.preventDefault();
        /* Act on the event */
        $(this).closest(".setname-list").find(".setname-item").removeClass('is-active').eq(j).addClass("is-active");
        $(this).closest(".settype-content").find(".set-inner").removeClass('is-active').eq(j).addClass("is-active");
        $('.section-meal-info').perfectScrollbar('update');
      });
    });
  });

  $(".btn-close-mealinfo").on('click', function(event) {
   event.preventDefault();
   /* Act on the event */
   $('html,body').animate({ scrollTop:( 0 )}, 1 );
   $(".section-meal-info").addClass("is-hide");
 });

  $(".btn-open-mealinfo").on('click', function(event) {
   event.preventDefault();
   /* Act on the event */
   $('.section-meal-info').animate({ scrollTop:( 0 )}, 1 );
   $(".section-meal-info").removeClass("is-hide");
 });

  $(".footer-foodlist").on('click', '.swiper-slide', function(event) {
    // event.preventDefault();
    /* Act on the event */
    var newFoodIndex = $(this).attr("data-index");
    // $(".category-item").eq(newFoodIndex).click();
    var newScroll = $(".category-name.anchor").eq(newFoodIndex).offset().top;
    $('html,body').animate({ scrollTop:( newScroll )}, 800 );
    footerSwiper.slideTo( newFoodIndex );
    // $(".footer-foodlist").find(".swiper-slide").removeClass('is-active').eq(newFoodIndex).addClass('is-active');
  });

  $(".section-meal-info").on('click', '.detail-info', function(event) {
    event.preventDefault();
    /* Act on the event */
    var _h = $(".section-meal-info .inner").height() + $("#footer").height();
    $('.section-meal-info .inner').animate({ scrollTop:( _h )}, 500 );
  });

  $('.section-meal-info .inner').perfectScrollbar();


})();
