'use strict';

  var scrollNow = $(window).scrollTop();

  $(window).scroll(function () {
    var scrollVal = $(this).scrollTop();
    var newOffset;
    var main_h = $(".section-main").height();
    var edm_h = $(".section-edm").find("img").eq(0).height();
    var side_h = $(".category-list").height()
    + parseInt( $(".category-list").css("padding-top") )
    + parseInt( $(".nav-category").css("padding-top") );
    var win_h = $(window).height();


  //Mobile UI
  if( ( scrollVal + win_h ) >= $("body").height() - $("#footer").height() ){
    // console.log("over!");
    $(".nav-footer").addClass("style-stick");
  }else{
    $(".nav-footer").removeClass("style-stick");
  }


  //Right-side stick to screen
  $(".section-edm").css("margin-top", scrollVal );
  if(scrollVal+edm_h > main_h){
    var _minus_h = scrollVal+edm_h - main_h;
    console.log("over!")
    $(".section-edm").css("margin-top", scrollVal - _minus_h );
  }

  //Left-side stick to screen
  if(scrollVal > scrollNow ){
    //when scrolling down

    if( side_h > win_h ){
      //when side is taller than windows : stick bottom
      if( (win_h+scrollVal) > side_h ){
        if( (win_h+scrollVal) <= main_h ){
          $(".category-list").css("margin-top", (scrollVal - side_h + win_h) );
        }
      }
    }else{
      //when side in shorter than windows : stick top
      $(".category-list").css("margin-top", scrollVal );
    }


  }else{

    //when scrolling up : stick top
    var _dis_scroll_up = scrollVal - scrollNow; //minus number
    var now_margin = $(".category-list").css("margin-top");
    now_margin = parseInt(now_margin);
    if( scrollVal <= (now_margin+_dis_scroll_up) ){
      $(".category-list").css("margin-top", scrollVal );
    }


  }

  scrollNow = scrollVal;
});
(function() {
  $(".section-edm").perfectScrollbar();
})();