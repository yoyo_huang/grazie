'use strict';
var stores = [];

function itemTemplate(lat,lng,name,tel,address,detailurl,pdfurl){

  var newItem = '<li class="searchstore-item" data-lat="' + lat + '" data-lng="' + lng + '">'
  + '<p class="name">' + name + '</p>'
  + '<p class="tel">' + tel + '</p>'
  + '<p class="address">' + address + '</p>'
  + '<div class="btn-wrap">'
  + '<a class="style-round-full" href="' + detailurl + '"><img src="../../images/icon-tag.svg" alt="">分店詳細資訊</a>'
  + '<a class="style-round-full" href="' + pdfurl + '"><img src="../../images/icon-book.svg" alt="">線上菜單</a>'
  + '</div>'
  + '</li>';
  return newItem;
}

$.getJSON('/javascripts/units/store.json', function(json, textStatus) {
  $.each(json, function(index, val) {
      // console.log(index);
      // console.log(val);
      stores[index] = {};
      stores[index].title = val.title;
      stores[index].region = val.region;
      stores[index].lat = val.lat;
      stores[index].lng = val.lng;
      stores[index].tel = val.tel;
      stores[index].address = val.address;
      stores[index].pdfurl = val.pdfurl;
      stores[index].detailurl = val.detailurl;

    });

}).success(function(){
  console.log("get json complete!");
  console.log( stores);
  var selectedRegionNum = $("#select-region").val();
  $(".searchstore-list").html('');
  $.each( stores, function(index, el) {
    if( stores[index].region == selectedRegionNum ){
      $(".searchstore-list").append( itemTemplate(stores[index].lat,stores[index].lng,stores[index].title,stores[index].tel,stores[index].address,stores[index].detailurl,stores[index].pdfurl) );
    }
  });
  
});

//SELECT REGION EVENT
$("body").find("#select-region").on('change', function(event) {
  // event.preventDefault();
  /* Act on the event */
  var selectedRegionNum = $(this).val();
  // console.log( $(this).val() );
  $(".searchstore-list").html('');

  $.each( stores, function(index, el) {
    if( stores[index].region == selectedRegionNum ){
      // console.log( stores[index] );
      $(".searchstore-list").append( itemTemplate(stores[index].lat,stores[index].lng,stores[index].title,stores[index].tel,stores[index].address,stores[index].detailurl,stores[index].pdfurl) );
    }
  });
});

(function() {

  $('.section-search').perfectScrollbar();

})();
