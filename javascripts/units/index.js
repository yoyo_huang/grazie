'use strict';

var scrollNow = $(window).scrollTop();
var nowScene = 1;
var offsetPoint= [];
$('html,body').animate({ scrollTop:( 0 )}, 800);

$(window).scroll(function () {
  var scrollVal = $(this).scrollTop();
  var win_h = $(window).height();
  var newOffset;

  if( ( scrollVal + win_h ) >= $("body").height() - $("#footer").height() ){
    // console.log("over!");
    $(".tag-career").addClass("style-stick");
  }else{
    $(".tag-career").removeClass("style-stick");
  }

  if( !($(window).width() > 480) ){
    $(".edm").addClass('is-slideup');
  }

  if( $(window).width() > 1024 && scrollNow == 0 && scrollVal > scrollNow ){
    // var newScroll = $(".section-scene2").offset().top;
    // scrollNow = newScroll;
    // $('html,body').animate({ scrollTop:( newScroll )}, 800 );
  }

  $(".section-scene").each(function(index, el) {
    if( scrollVal + $(window).height()/3*2 >= offsetPoint[index] ){
      if( scrollVal + $(window).height()/3*2 < offsetPoint[index+1] ){
        nowScene = index+1 ;
      }
    }
  });
  if( scrollVal + $(window).height()/3*2 >= offsetPoint[ ( ($(".section-scene").length) - 1 ) ] ){
    nowScene = $(".section-scene").length ; //Last scene.
  }
  if( scrollVal + $(window).height() + 120 >= $("body").height()  ){
    nowScene = $(".section-scene").length ; //Last scene. (when almost hit the page bottom)
  }

  console.log(nowScene);
  $(".section-scene"+nowScene).find(".style-fadescroll").removeClass('status-off');
  scrollNow = scrollVal;
});

$(window).resize(function(){
  $(".section-scene1").css("max-height", $(window).height() );    
  $(".edm").css("max-height", $(window).height() );    
});

$(".edm img").on('load', function(event) {
  if( !($(window).width() >480) ){
    setTimeout(function(){ $(".edm").addClass('is-slideup'); }, 2000);
  }
});

$(".goto-scene2").on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  var newScroll = $(".section-scene2").offset().top;
  $('html,body').animate({ scrollTop:( newScroll )}, 800);
});

$(window).load(function() {
  $(".section-scene").each(function(index, el) {
    offsetPoint.push( $(this).offset().top );  
  });
  // console.log("offsetPoint = " + offsetPoint);
});

(function() {
  // alert( $(window).height() );
  $(".section-scene1").css("max-height", $(window).height() );
  $(".edm").css("max-height", $(window).height() );    

  $(".btn-zoom").on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    $(".edm").toggleClass('is-slideup');
  });

  var bgSwiper = new Swiper('.bg-slider', {
    effect: 'fade',
    speed: 1000,
    autoplay: 8000
  });
  
})();
