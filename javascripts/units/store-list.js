'use strict';
var stores = [];
var myLatlng = [];
var marker = [];

//EX: pagename.html?a=xxx&b=yyy
function searchToObject() {
  var pairs = window.location.search.substring(1).split("&"),
    obj = {},
    pair,
    i;
  for ( i in pairs ) {
    if ( pairs[i] === "" ) continue;

    pair = pairs[i].split("=");
    obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
  }

  return obj;
}
var searchObj = searchToObject();

function hoverMarker(Lat,Lng){
  // alert("hovering marker" + myLat + "," + myLng + "!");
  $("body").find(".searchstore-item").each(function(index, el) {

    if( $(this).attr("data-lat") == Lat ){
      if( $(this).attr("data-lng") == Lng ){
        $(this).addClass("is-active");
      }else{
        // $(this).removeClass("is-active");
      }
    }
  });
}

function itemTemplate(lat,lng,name,tel,address,detailurl,pdfurl){

  var newItem = '<li class="searchstore-item" data-lat="' + lat + '" data-lng="' + lng + '">'
  + '<p class="name">' + name + '</p>'
  + '<p class="tel">' + tel + '</p>'
  + '<p class="address">' + address + '</p>'
  + '<div class="btn-wrap">'
  + '<a class="style-round-full" href="' + detailurl + '"><img src="../../images/icon-tag.svg" alt="">分店詳細資訊</a>'
  + '<a class="style-round-full" href="' + pdfurl + '"><img src="../../images/icon-book.svg" alt="">線上菜單</a>'
  + '</div>'
  + '</li>';
  return newItem;
}

function initializemap() {
// Create an array of styles.
var styles = [
{
  elementType: "labels.text.fill",
  stylers: [
  { color: "#333333" }
  ]
}
];
  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});

  //台灣全景
  var mapOptions = {
    zoom: 8,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    },
    center: {lat: 23.6, lng: 121},
    mapTypeControl: true,
    scrollwheel: true,
    zoomControl: true,
    scaleControl: true,
    streetViewControl:true
  };
  
  // 初始化地圖
  var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

  var markerimage = {
    url: '/images/icon-marker-40.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(40, 40),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(20, 20)
    };

  var markerimageXL = {
    url: '/images/icon-marker-60.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(60, 60),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(30, 30)
    };

    /* 每new google.maps.Marker一個var就會出現一個地標 */

    $.getJSON('/javascripts/units/store.json', function(json, textStatus) {
      $.each(json, function(index, val) {
      // console.log(index);
      // console.log(val);
      stores[index] = {};
      stores[index].title = val.title;
      stores[index].region = val.region;
      stores[index].lat = val.lat;
      stores[index].lng = val.lng;
      stores[index].tel = val.tel;
      stores[index].address = val.address;
      stores[index].pdfurl = val.pdfurl;
      stores[index].detailurl = val.detailurl;

    });

    }).success(function(){
      console.log("get json complete!");
      // console.log( stores);

      $.each( stores, function(index, el) {
        // 初始化地標
        myLatlng[index] = new google.maps.LatLng( stores[index].lat, stores[index].lng );
        marker[index] = new google.maps.Marker({
          position: myLatlng[index],
          icon: markerimage,
          animation: google.maps.Animation.DROP,
          map: map,
          title: stores[index].title
        });

         marker[index].addListener('mouseover', function(){
          marker[index].setIcon(markerimageXL);
          var Lat = parseFloat( marker[index].getPosition().lat() ).toFixed(7);
          var Lng = parseFloat( marker[index].getPosition().lng() ).toFixed(7);
          // console.log(Lat+','+Lng);       
          $("body").find(".searchstore-item").each(function(index, el) {
            var thisLat = parseFloat( $(this).attr("data-lat") ).toFixed(7);
            var thisLng = parseFloat( $(this).attr("data-lng") ).toFixed(7);
            // console.log(thisLat + ',' + thisLng);
            if( thisLat == Lat && thisLng == Lng ){
              $(this).addClass('is-active');
              // newUrl = $(this).find(".style-round-full").eq(0).attr("href");

              // console.log( $(this).find(".style-round-full").eq(0).attr("href") );
            }else{
              $(this).removeClass('is-active');
            }
          });
          // marker[index].setAnimation(google.maps.Animation.BOUNCE);
         }); //End of marker mouseover event.

         marker[index].addListener('mouseout', function(){
          marker[index].setIcon(markerimage);
          marker[index].setAnimation(null);
          $("body").find(".searchstore-item").removeClass('is-active');
         }); //End of marker mouseout event.

         marker[index].addListener('click', function(){
          var Lat = parseFloat( marker[index].getPosition().lat() ).toFixed(7);
          var Lng = parseFloat( marker[index].getPosition().lng() ).toFixed(7);

          $.each( stores, function(index, el) {
            var lat_i = parseFloat( stores[index].lat ).toFixed(7);
            var lng_i = parseFloat( stores[index].lng ).toFixed(7);
            var newUrl = "#";
            if( lat_i == Lat && lng_i == Lng ){
              newUrl = stores[index].detailurl;
              window.location.href = newUrl;
            }
          });
         }); //End of marker click event.

      }); //End of each stores[]

      //Get Url search value to change content.
      var getRegion = parseInt(searchObj.region)>0 ? parseInt(searchObj.region) : 0;
      console.log("region = "+getRegion);
      // copy from change-event of #select-region
      if( getRegion > 0){
          $("body").find("#select-region").find("option[value="+getRegion+"]").attr("selected",true);
          $(".searchstore-list").html('');
          var storeNum = 0;
          var latSum = 0;
          var lngSum = 0;
          var center = {};
          $.each( stores, function(index, el) {
            if( stores[index].region == getRegion ){
              storeNum ++;
              latSum += stores[index].lat;
              lngSum += stores[index].lng;                
              $(".searchstore-list").append( itemTemplate(stores[index].lat,stores[index].lng,stores[index].title,stores[index].tel,stores[index].address,stores[index].detailurl,stores[index].pdfurl) );
              $('.searchstore-list').perfectScrollbar('update');
            }
          });
          center.lat = latSum / storeNum;
          center.lng = lngSum / storeNum;
          map.setZoom(13);
          map.setCenter(center);
        }
    
    });

    //SELECT REGION EVENT
    $("body").find("#select-region").on('change', function(event) {
    // event.preventDefault();
    /* Act on the event */
    var selectedRegionNum = $(this).val();

    if( selectedRegionNum != 0){
      // console.log( $(this).val() );
      $(".searchstore-list").html('');      
      var storeNum = 0;
      var latSum = 0;
      var lngSum = 0;
      var center = {};
      $.each( stores, function(index, el) {
        if( stores[index].region == selectedRegionNum ){
          // console.log( stores[index] );
          storeNum ++;
          latSum += stores[index].lat;
          lngSum += stores[index].lng;
          
          $(".searchstore-list").append( itemTemplate(stores[index].lat,stores[index].lng,stores[index].title,stores[index].tel,stores[index].address,stores[index].detailurl,stores[index].pdfurl) );
          $('.searchstore-list').perfectScrollbar('update');
        }
      });
      center.lat = latSum / storeNum;
      center.lng = lngSum / storeNum;

      map.setZoom(13);
      map.setCenter(center);
    }
    
  });

  //HOVER STORE EVENT
  
  $("body").on('mouseover', '.searchstore-item', function(event) {
    event.preventDefault();
    /* Act on the event */
    var myLat = $(this).attr("data-lat");
    var myLng = $(this).attr("data-lng");

    for (var i = 0; i < marker.length; i++) {
      // console.log( marker[i].getPosition().lat() );
      // console.log( marker[i].getPosition().lng() );
      if( marker[i].getPosition().lat() != myLat && marker[i].getPosition().lng() != myLng ){
        // marker[i].setMap(null);
      }else{
         marker[i].setIcon(markerimageXL);
         // marker[i].setAnimation(google.maps.Animation.BOUNCE);
      }
    }

  });
  $("body").on('mouseout', '.searchstore-item', function(event) {
    event.preventDefault();
    /* Act on the event */
    for (var i = 0; i < marker.length; i++) {
      marker[i].setMap(map);
      marker[i].setIcon(markerimage);
      marker[i].setAnimation(null);
    }
  });


  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
  // map.setOptions({ scrollwheel: false });
}


google.maps.event.addDomListener(window, 'load', initializemap());

(function() {
  // console.log(searchObj);
  $('.section-search').perfectScrollbar();
})();
