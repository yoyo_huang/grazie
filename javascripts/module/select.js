  'use strict';

(function() {

  //custom select 
  $("body").on('click', '.module-customselect:not(.readonly)', function() {
    $(".module-customselect.is-on").not(this).removeClass('is-on');
    $(this).toggleClass('is-on');
  });
  $("body").on('click', '.module-customselect li', function() {
    var mytext = $(this).text();
    var myvalue = $(this).val();
    $(this).parent().parent().find('.module-customselect-selected').text(mytext).next("input").val(myvalue);
  });
  //CLOSE ALL SELECTS ON CLICKING ANYOTHERS
  $( "body" ).click(function( event ) {
    //whether himself
    if ( event.target.nodeName == "DIV" && event.target.className != "module-customselect" ){
      $("body").find(".module-customselect").removeClass('is-on');
    }
    //whether his child
    else if( event.target.nodeName == "UL" || event.target.nodeName == "LI" || event.target.nodeName == "SPAN" ){
      if ( ! $(event.target).parents(".module-customselect") ){
        $("body").find(".module-customselect").removeClass('is-on');
      }
    //any others
    }else if ( event.target.nodeName != "DIV" && event.target.nodeName != "UL" && event.target.nodeName != "LI" && event.target.nodeName != "SPAN" ){
      $("body").find(".module-customselect").removeClass('is-on');
    }
  });


})();