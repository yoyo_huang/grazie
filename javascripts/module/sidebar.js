'use strict';

(function() {

  var scrollNow,
      innerH,
      pageH,
      _h,
      _t,
      nowSideBottom,
      content_h,
      maxAddTop;

  function getSidebarVar(){
    innerH = window.innerHeight;
    pageH = $("body").height();
    _h = $(".section-main").find(".category-list").height();
    _t = $(".section-main").offset().top;
    nowSideBottom = parseInt(_t) + parseInt(_h);
    content_h = parseInt( $(".main-foodlist").height() );
    maxAddTop = content_h - parseInt(_h);
    console.log(
      "scrollTop = "+$(window).scrollTop()+
      " ,innerH = "+innerH+
      " , pageH = "+pageH+
      " , _h = "+_h+
      " , _t = "+_t+    
      " , nowSideBottom = "+nowSideBottom+
      " , content_h = "+content_h+
      " , maxAddTop = "+maxAddTop
    );
  }
  $(window).resize(function(){
    getSidebarVar();
  });
  $(window).scroll(function () {
    getSidebarVar();
    var scrollVal = parseInt($(this).scrollTop());
    var newOffset = scrollVal + innerH;

      if ( maxAddTop > 0 ){
        //Only Follow Screen When content is taller than sidebar
        if ( newOffset > nowSideBottom ){
          var addTop = newOffset - nowSideBottom;
          // console.log("Overflowed. addTop : "+addTop);
          if (addTop < maxAddTop){
            $(".section-main").find(".category-list").css("margin-top",addTop);
            console.log("addTop:" + addTop);
          }
          else{
            $(".section-main").find(".category-list").css("margin-top",maxAddTop);            
          }
        }
        else{
          // console.log("Not Overflowed.");
          $(".section-main").find(".category-list").css("margin-top",0);
        }

      }
      scrollNow = scrollVal;

  });


})();